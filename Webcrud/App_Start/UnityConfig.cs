using System.Web.Http;
using Unity;
using Unity.WebApi;
using Webcrud.Interface;
using Webcrud.Services;

namespace Webcrud
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IProduct, ProductService>();
            container.RegisterType<ICategory, CategoryService>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}