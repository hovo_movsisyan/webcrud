﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webcrud.Interface;
using Webcrud.Models;

namespace Webcrud.Services
{
    public class ProductService : IProduct
    {
        private ShopContext context = new ShopContext();

        public void AddProduct(Product product)
        {
            try
            {
                context.Products.Add(new Product
                {
                    ProductName = product.ProductName,
                    Price=product.Price,
                    Description = product.Description,
                    CategoryId = product.CategoryId
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteProduct(int id)
        {
                Product product = context.Products.Find(id);

            try
            {
                if (product != null)
                {
                    context.Products.Remove(product);
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Product GetProduct(int id)
        {
            try
            {
                var product = context.Products.Where(x => x.ProductId == id).FirstOrDefault();
                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Product> GetProducts()
        {
            var products = context.Products.ToList();
            return products;
        }

        public void UpdateProduct(Product product, int prodId)
        {
            var getproduct = context.Products.Where(x => x.ProductId == prodId).FirstOrDefault();
            try
            {
                if (getproduct != null)
                {
                    getproduct.ProductName = product.ProductName;
                    getproduct.Price = product.Price;
                    getproduct.Description = product.Description;
                    getproduct.CategoryId = product.CategoryId;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}