﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webcrud.Interface;
using Webcrud.Models;

namespace Webcrud.Services
{
    public class CategoryService : ICategory
    {
        private ShopContext context = new ShopContext();

        public void AddCategory(Category category)
        {
            try
            {
                context.Categories.Add(new Category
                {
                    CategoryName = category.CategoryName,
                    Description = category.Description
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteCategory(int id)
        {
            Category category = context.Categories.Find(id);
            try
            {
                if (category != null)
                {
                    context.Categories.Remove(category);
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Category> GetCategories()
        {
            var categories = context.Categories.ToList();
            return categories;
        }

        public Category GetCategory(int id)
        {
            try
            {
                var category = context.Categories.Where(x => x.CategoryId == id).FirstOrDefault();
                return category;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateCategory(Category category, int categoryId)
        {
            var getcategory = context.Categories.Where(x => x.CategoryId == categoryId).FirstOrDefault();
            try
            {
                if (getcategory != null)
                {
                    getcategory.CategoryName = category.CategoryName;
                    getcategory.Description = category.Description;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}