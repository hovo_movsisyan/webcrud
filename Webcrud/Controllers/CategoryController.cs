﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webcrud.Interface;
using Webcrud.Models;

namespace Webcrud.Controllers
{
    public class CategoryController : ApiController
    {

        private readonly ICategory _category;

        public CategoryController(ICategory category)
        {
            _category = category;
        }

        [HttpGet]
        public IEnumerable<Category> GetCategories() => _category.GetCategories();

        [HttpGet]
        public Category GetCategory(int id) => _category.GetCategory(id);

        [HttpPost]
        public IHttpActionResult AddCategory(Category category)
        {
            _category.AddCategory(category);
            return Ok(new { message = "category is added" });
        }

        [HttpPost]
        public IHttpActionResult UpdateCategory(Category category, int id)
        {
            _category.UpdateCategory(category, id);
            return Ok(new { message = "category is updated" });
        }

        [HttpDelete]
        public IHttpActionResult DeleteCategory(int id)
        {
            _category.DeleteCategory(id);
            return Ok(new { message = "category is deleted" });
        }
    }
}
