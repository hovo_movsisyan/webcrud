﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webcrud.Interface;
using Webcrud.Models;

namespace Webcrud.Controllers
{
    public class ProductController : ApiController
    {
        private readonly IProduct _product;

        public ProductController(IProduct product)
        {
            _product = product;
        }

        [HttpGet]
        public IEnumerable<Product> GetProducts() => _product.GetProducts();

        [HttpGet]
        public Product GetProduct(int id) => _product.GetProduct(id);

        [HttpPost]
        public IHttpActionResult AddProduct(Product product)
        {
            _product.AddProduct(product);
            return Ok(new { message = "product is added" });
        }

        [HttpPost]
        public IHttpActionResult UpdateProduct(Product product, int id)
        {
            _product.UpdateProduct(product, id);
            return Ok(new { message = "product is updated" });
        }

        [HttpDelete]
        public IHttpActionResult DeleteProduct(int id)
        {
            _product.DeleteProduct(id);
            return Ok(new { message = "product is deleted" });
        }
    }
}
