﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Webcrud.Models
{
    public class ShopContext:DbContext
    {
        public ShopContext():base("constring") {}

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }


    }
}